import modules.Mesh1D as mesh
import numpy as np
import matplotlib.pyplot as plt

n_el = 100
d_initial = (200) * np.ones([n_el+1, 1])
d_initial[-1, 0] = 0

sim = mesh.Mesh1D(n_el=n_el,
                  x_start=0,
                  x_end=0.02,
                  d_init=d_initial,
                  rhoc=10e6,
                  k=10,
                  S=0,
                  T_L=None,
                  T_R=0,
                  q_L=0,
                  q_R=None,
                  alpha=0,
                  CFL=0.8,
                  t_final=120)

sim.solve(3)

plt.plot(sim.x_nodes, sim.d, label="Mass lumped FEM model")
plt.grid()
plt.show()