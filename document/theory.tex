\section{Theory}
This section covers some of the theory that is relevant to the problem presented in section \ref{sec:problem}.
\subsection{Mathematical descriptions of the problem}
The problem can be described in various mathematical forms. This subsection displays some of these forms. However, it neglects the mathematical steps taken to get from one form to another.
\subsubsection{Strong form}
The strong form of the problem is: find $T$ that satisfies
\begin{equation}
\begin{gathered}
\dfrac{\partial T}{\partial t} = \dfrac{k}{\rho c}\dfrac{\partial^2 T}{\partial x^2} \quad \text{on} \;\; x  \in [0,L], \\
T = 0 \quad \text{on} \;\; x = L, \\
\dfrac{\partial T}{\partial x} = 0 \quad \text{on} \;\; x = 0, \\
T(x,0) = 200.
\end{gathered}
\end{equation}

\subsubsection{Weak form}
Without derivation the weak form is presented to be: find $T\in H^1$ that satisfies
\begin{equation}
\rho c \int_0^L w\dfrac{\partial T}{\partial t} dx + k \int_0^L\dfrac{\partial w}{\partial x} \dfrac{\partial T}{\partial x} dx = 0,
\end{equation}
where the test function $w \in H^1$, $w|_{x=L}=0$, $\forall w$.
\subsubsection{Finite Element form \label{sec:FE form}}
Finally the Finite Element (FE) form is 
\begin{equation}
\begin{gathered}
\underbrace{\rho c \int_0^L \bm{N}^T\bm{N} dx}_{\bm{M}} \dot{\bm{d}} + \underbrace{k \int_0^L\bm{B}^T\bm{B} dx}_{\bm{K}} \bm{d} = \bm{0},\\
\bm{M}\dot{\bm{d}} + \bm{Kd}= \bm{0}.
\label{eq:equilibrium}
\end{gathered}
\end{equation}
Where $\bm{M}$ is the mass matrix and $\bm{K}$ is the stiffness matrix. Furthermore,
\begin{equation}
\begin{gathered}
T \approx \bm{N}\bm{d}, \quad \dfrac{\partial T}{\partial x} \approx \bm{B}\bm{d}, \\
\bm{B} = \dfrac{\partial}{\partial x} \bm{N}  
\end{gathered}
\end{equation}
and $\bm{N}$ is a vector of the Lagrange basis functions such that
\begin{equation}
N_k = \dfrac{\prod_{i=1, i\neq k}^n(x-x_i)}{\prod_{i=1, i\neq k}^n(x_k-x_i)},
\end{equation}
where $n-1$ is the order of the Lagrange basis (i.e. a linear element, $1^{st}$-order, would mean $n=2$) and the subscripts denote the nodal numbers.
\subsection{Generalised $\alpha$-scheme manipulation}
The generalised $\alpha$-scheme is a method of integrating a temporally discretised problem. It is defined by the equation
\begin{equation}
\bm{d}_{n+1} = \bm{d}_{n} + \Delta t(1-\alpha) \dot{\bm{d}}_{n} + \alpha \Delta t \dot{\bm{d}}_{n+1},
\label{eq:gen alpha}
\end{equation}
where subscript \textbullet$ $ denotes the value at time-step \textbullet, $\Delta t$ is the time-step size and $\alpha\in[0,1]$. Rearranging the equation leads to 
\begin{equation}
\dot{\bm{d}}_{n+1} = \dfrac{1}{\alpha \Delta t } (\bm{d}_{n+1} -\bm{d}_{n} - \Delta t(1-\alpha) \dot{\bm{d}}_{n}).
\label{eq:gen rearrange}
\end{equation}
Taking equilibrium at time-step $n+1$ and substituting equation \ref{eq:gen rearrange} into equation \ref{eq:equilibrium} gives
\begin{equation}
\begin{gathered}
\bm{M}\dfrac{1}{\alpha \Delta t } (\bm{d}_{n+1} -\bm{d}_{n} - \Delta t(1-\alpha) \dot{\bm{d}}_{n}) + \bm{K}\bm{d}_{n+1}= \bm{0}.
\end{gathered}
\label{eq:half way}
\end{equation}
Taking equilibrium at time-step $n$ gives rise to the equation
\begin{equation}
\dot{\bm{d}}_n = -\bm{M}^{-1}\bm{K}\bm{d}_n.
\label{eq:ddotn}
\end{equation}
Equation \ref{eq:ddotn} is then substituted into equation \ref{eq:half way} and simplified to give
\begin{equation}
\begin{gathered}
\bm{M}\dfrac{1}{\alpha \Delta t } (\bm{d}_{n+1} -\bm{d}_{n} + \Delta t(1-\alpha) \bm{M}^{-1}\bm{K}\bm{d}_n) + \bm{K}\bm{d}_{n+1}= \bm{0},\\
(\bm{M} + \alpha \Delta t \bm{K}) \bm{d}_{n+1} = (\bm{M} - \Delta t(1-\alpha)\bm{K})\bm{d}_n.
\end{gathered}
\end{equation}
Hence, the solution at time-step $n+1$ can be found as a function purely of the solution at time-step $n$ for any $\alpha$-scheme. This implementation of the $\alpha$-scheme is beneficial to many others, as one need not calculate nor store $\dot{\bm{d}}_n$ at any stage, which most other implementations require. This reduces the storage cost of the program and significantly reduces the computational costs, as solving for $\dot{\bm{d}}_n$ requires solving a system of linear equations which is typically the most computationally expensive procedure in FEA.

\subsection{Critical time-step size \label{sec: time size}}
The system of equations presented in equation \ref{eq:equilibrium} can be decoupled through diagonalisation, leading to the set of equations 
%\footnote{Conventional index notation has been used.}
\begin{equation}
\dot{z}_i + \lambda_iz_i=0,
\label{eq:diag}
\end{equation}
where $\lambda_i$ is the eigenvalue of the corresponding eigenvector $\bm{\Psi}_i$ of the eigenvalue problem
\begin{equation}
(\bm{K} - \lambda \bm{M})\bm{\Psi} = \bm{0}
\end{equation}
 and 
\begin{equation}
\dot{z}_i = (\bm{\Psi_}{j})_i \dot{d_j} \;\; \text{and} \;\; z_i = (\bm{\Psi_}{j})_i d_j.
\end{equation}
If equation \ref{eq:diag} is temporally discretised and a generalised $\alpha$-scheme is used, it can be shown that
\begin{equation}
z^{(n+1)}_i = \dfrac{1- \lambda_i \Delta t(1-\alpha)}{1 + \lambda_i\alpha\Delta t} z_i^{(n)}
\label{eq:amp}
\end{equation}
where superscript (\textbullet)\footnote{This is the only subsection where the time-step position will be displayed this way. For the remainder of the report the time-step position will be shown in the subscript, as per the previous subsection. It is shown this way here to free up the subscript for index notation.} denotes the value at time-step \text{\textbullet}. For the magnitude of the solution to not tend to infinity (i.e. for the solution to remain stable) it is required that
\begin{equation}
\left| \dfrac{1- \lambda_i \Delta t(1-\alpha)}{1 + \lambda_i\alpha\Delta t} \right| \leq 1.
\end{equation}
By manipulating this inequality it can be shown that, for stability, if $0\leq \alpha <0.5$
\begin{equation}
 0\leq  \Delta t \leq \Delta t_{crit}:= \dfrac{2}{(1-2\alpha)\max_i(\lambda_i)},
 \label{eq:stab 0}
\end{equation}
\footnote{Although the scheme would be stable for $\Delta t=0$, this is not a useful time-step size as the solution would remain constant for the duration of the simulation as can be seen by equation \ref{eq:amp}. This also makes physical sense, because if $\Delta t=0$ the simulation stays at the initial time and hence the solution should not change.} where $\Delta t_{crit}$ is known as the critical time-step size. Additionally, if $0.5 \leq \alpha \leq 1$
\begin{equation}
0\leq \Delta t.
\label{eq:stab 1}
\end{equation}
It is easy to satisfy inequality \ref{eq:stab 1}, however ensuring that inequality \ref{eq:stab 0} is satisfied requires the use of the maximum eigenvalue of the system. Finding eigenvalues for large systems of equations is a very computationally expensive process, however this problem can be avoided by estimating the maximum eigenvalue using the inequality
\begin{equation}
\max_i(\lambda_i) \leq \max_e(\max_j(\lambda_j^e))
\end{equation}
where $\max_j(\lambda_j^e)$ is the maximum eigenvalue of the associated element level eigenvalue problem 
\begin{equation}
(\bm{k}^e - \lambda^e \bm{m}^e)\bm{\psi}^e = \bm{0},
\end{equation}
where $\bm{k}^e$ is the element level stiffness matrix, $\lambda^e$ is the element level eigenvalue, $\bm{m}^e$ is the element level mass matrix and $\bm{\psi}^e$ is the element level eigenvector. Hence, by solving the (far smaller) eigenvalue problem for all elements and finding the maximum of these eigenvalues, the maximum eigenvalue of the system can be approximated and one can ensure that inequality \ref{eq:stab 0} is satisfied. In the case where all the elements in the system are identical this need only be done for one element, which is far more computationally efficient.\\
\\
As a final comment, note that the coupled set of equations is a linear combination of the decoupled set of equations and vice-versa. Hence, if stability is satisfied for the decoupled set of equations, then it will also be satisfied for the coupled set of equations and vice-versa.
\subsection{Error analysis \label{sec: error analysis}}
It is important to be able to quantify the error of a simulation so that different methods or schemes can be compared to each other. This subsection sets out a frame work that can be used to quantify the error in the context of this assignment.\\
\\
The error at the nodal positions at time-step $n$, $\bm{e}_n$, can be calculated using
\begin{equation}
\bm{e}_n = \bm{T}(\overline{\bm{x}}, t_n) - \bm{d}_n.
\end{equation}
Where $\bm{T}$ is function producing a vector of the value of the analytical solution at the nodal positions, $\overline{\bm{x}}$, at time $t_n$. The error at any point in the domain can then be approximated using the global shape functions
\begin{equation}
e_n \approx \bm{Ne}_n,
\end{equation}
and the square of the error can be approximated using
\begin{equation}
e^2_n \approx \bm{e}^T_n\bm{N}^T\bm{Ne}_n.
\end{equation}
The $L_2$-norm of the error, $||e||_{L_2}$, can then be approximated as
\begin{equation}
||e||_{L_2} := \sqrt{\int_0^Le^2dx} \approx \sqrt{\bm{e}^T\int_0^L\bm{N}^T\bm{N}dx\bm{e}}.
\end{equation}
Hence, the $L_2$-norm of the error can be conveniently approximated using the already calculated mass matrix, $\bm{M}$
\begin{equation}
||e||_{L_2} \approx \sqrt{\dfrac{1}{\rho c}\bm{e}^T\bm{M}\bm{e}}.
\end{equation}
Additionally, the error is related to the mesh size by the equation
\begin{equation}
||e||_{L_2} = \beta \left(\dfrac{1}{N_{el}}\right)^p,
\end{equation}
where $N_{el}$ is the number elements in the simulation, $p$ is the rate of convergence of the scheme and $\beta$ is some constant. Taking the log of both sides of the equation leads to
\begin{equation}
\log(||e||_{L_2}) = p \log \left(\dfrac{1}{N_{el}}\right) + \log(\beta).
\end{equation}
Hence, the convergence rate of a scheme can be determined by plotting the log of the $L_2$-norm of the error against the log of $\dfrac{1}{N_{el}}$ and calculating the gradient.

\subsection{Special mass-lumping \label{sec:lump}}
Mass-lumping is a family of techniques whereby a diagonal mass-lumped matrix $\bm{M}_L$ is constructed from the consistent mass matrix, $\bm{M}$. There are various ways that this can be done, but the technique that will be presented and utilised in this assignment is \textit{special mass-lumping}. Special mass-lumping uses the total mass, $m$, calculated by
\begin{equation}
m = \sum_{i,j=1}^{n_{dof}} M_{ij}
\end{equation}
where $n_{dof}$ is the number of degrees of freedom, and the trace of the mass matrix, $s= \text{tr}(\bm{M})$, to determine a scaling factor, $\gamma$, such that
\begin{equation}
\gamma = \dfrac{m}{s}.
\end{equation}
The mass-lumped matrix is then determined using
\begin{equation}
(M_L)_{ij} = \gamma M_{ij}\delta_{ij}
\end{equation}
