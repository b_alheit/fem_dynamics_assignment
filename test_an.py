import modules.Analytical_solution as AS
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 0.02, 200)
rhoc = 10e6
k = 10

T = AS.T_an(x, 1, k/rhoc, 20)

plt.plot(x, T)
plt.grid()
plt.show()