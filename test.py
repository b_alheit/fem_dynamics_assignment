import numpy as np
import scipy.linalg

M = np.array([[1, 2, 3],
              [2, 5, 2],
              [3, 2, 3]
              ])

K = np.array([[5, 6, 0],
              [6, 3, 2],
              [0, 2, 3]
              ])

I = np.eye(3)
# print(np.linalg.inv(M))
A = np.matmul(np.linalg.inv(M), K)

eig = np.linalg.eigvals(A)
eigvals = scipy.linalg.eigvals(K, M)

print("Eig vals: ", eig)
print("Eig vals: ", eigvals)

print(np.linalg.det(K - eig[0] * M))
print(np.linalg.det(K - eig[1] * M))
print(np.linalg.det(K - eig[2] * M))

eig1 = np.linalg.eigvals(K)

print("Eig vals: ", eig1)

print(np.linalg.det(K - eig1[0] * I))


eig2 = np.linalg.eigvals(M)

print("Eig vals: ", eig2)

print(np.linalg.det(M - eig2[0] * I))

