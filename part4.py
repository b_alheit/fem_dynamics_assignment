import modules.Mesh1D as mesh
import numpy as np
import modules.Analytical_solution as AS
import matplotlib.pyplot as plt

n_el = 100  # Number of elements used
d_initial = 200 * np.ones([n_el+1, 1])  # Initial conditions
d_initial[-1, 0] = 0
t = 120     # Time to solve to

figsize = 0.6

# Creating a mass-lumped simulation
sim = mesh.Mesh1D(n_el=n_el,
                  x_start=0,
                  x_end=0.02,
                  d_init=d_initial,
                  rhoc=10e6,
                  k=10,
                  S=0,
                  T_L=None,
                  T_R=0,
                  q_L=0,
                  q_R=None,
                  alpha=0,
                  CFL=0.8,
                  t_final=t,
                  lump=True)    # Note the lumping boolean is set to True

sim.solve(3)    # Solving the simulation

# Creating the exact solution
rhoc = 10e6
k = 10
x = np.linspace(0, 0.02, 200)
T = AS.T_an(x, t, k / rhoc, 100)


plt.plot(x, T, label="Analytical solution", linewidth=4)
plt.plot(sim.x_nodes, sim.d, label="Mass lumped FEM model", markerfacecolor="None", marker="o")
plt.grid()
plt.legend()
plt.xlim(0, 0.02)
plt.xlabel("x (m)")
plt.ylabel("T ($^o$C)")
plt.title("Temperature distribution at " + str(t) + " $s$ using Forward Euler time-stepping")
plt.figure(1).set_figheight(figsize*9)
plt.figure(1).set_figwidth(figsize*13)
plt.figure(1).tight_layout()
plt.savefig("./document/figures/part4.pdf")
plt.show()
