import modules.Mesh1D as mesh
import numpy as np
import modules.Analytical_solution as AS
import matplotlib.pyplot as plt

t = 120     # Time to solve to
n_el = np.array([10, 20, 40, 80])   # Number of elements used
alpha = [0.5, 1]    # Alpha values to be used
names = ["Crank-Nicolson", "Backward Euler"]    # Scheme names
style = ["--", "-.", ":"]   # Line styles   for plotting
markers = ["^", "o", "s"]   # Marker styles for plotting

x = np.linspace(0, 0.02, 200)
rhoc = 10e6
k = 10
figsize = 0.6

err = np.zeros([len(alpha), len(n_el)])

for j in range(len(alpha)):

    for i in range(len(n_el)):

        # Creating an array of exact solution values at the same position as the nodal points
        x_comp = np.linspace(0, 0.02, n_el[i]+1)
        T_comp = AS.T_an(x_comp, t, k / rhoc, 100)

        # Creating the initial conditions
        d_initial = 200 * np.ones([n_el[i] + 1, 1])
        d_initial[-1, 0] = 0

        # Creating the simulation
        sim = mesh.Mesh1D(n_el=n_el[i],
                      x_start=0,
                      x_end=0.02,
                      d_init=d_initial,
                      rhoc=rhoc,
                      k=k,
                      S=0,
                      T_L=None,
                      T_R=0,
                      q_L=0,
                      q_R=None,
                      alpha=alpha[j],
                      CFL=1,
                      t_final=t)

        sim.solve(3)    # Solving the simulation
        error = np.array([T_comp - sim.d.flatten()]).T  # Creating the error array
        print(np.sum(np.abs(error)))
        err[j, i] = np.sqrt(np.matmul(error.T, np.matmul(sim.M/sim.rhoc, error)))[0, 0]     # Calculating and storing the L2-norm of the error
    print("_______________________")
print(err)

# Creating arrays of the log of the L2 error
logerr0 = np.log(err[0, :])
logerr1 = np.log(err[1, :])

# Creating an array of the log of 1/the number of elements
lognel = np.log(1/n_el)

# Calculating the log log gradients
grad0 = (logerr0[0] - logerr0[-1]) / (lognel[0] - lognel[-1])
grad1 = (logerr1[0] - logerr1[-1]) / (lognel[0] - lognel[-1])

print(names[0], " grad: ", grad0)
print(names[1], " grad: ", grad1)

plt.loglog(1/n_el, err[0, :], marker="^", markerfacecolor="None", label=names[0] + ", grad$\\approx$ " + str(grad0)[:5])
plt.loglog(1/n_el, err[1, :], marker="o", markerfacecolor="None", label=names[1] + ", grad$\\approx$ " + str(grad1)[:5])
plt.grid()
plt.legend()
plt.ylabel("$||e||_{L_2}$")
plt.xlabel("$\\frac{1}{N}$")
plt.title("Convergence study of $\\alpha$-family schemes")
plt.figure(1).set_figheight(figsize*9)
plt.figure(1).set_figwidth(figsize*13)
plt.figure(1).tight_layout()
plt.savefig("./document/figures/part3.pdf")
plt.show()
