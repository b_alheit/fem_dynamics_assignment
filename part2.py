import modules.Mesh1D as mesh
import numpy as np
import modules.Analytical_solution as AS
import matplotlib.pyplot as plt

t = [40, 80, 120]  # times to plot solutions at
n_el = 20   # number of elements used
d_initial = 200 * np.ones([n_el+1, 1])    # Initial temperature values
d_initial[-1, 0] = 0    # Setting the right face to zero
alpha = [0, 0.5, 1]     # Creating a list of alpha values to be used
names = ["Forward Euler", "Crank-Nicolson", "Backward Euler"]   # Names of time-stepping schemes used
style = ["--", "-.", ":"]   # Linestyles to be used when plotting
markers = ["^", "o", "s"]   # Markers to be used when plotting

CFL = 1.2   # Critical time-step size scaling factor

x = np.linspace(0, 0.02, 200)
x_comp = np.linspace(0, 0.02, 21)
rhoc = 10e6
k = 10
figsize = 0.7

err = np.zeros([len(t), len(alpha)])

for j in range(len(t)):
    T = AS.T_an(x, t[j], k/rhoc, 100)
    T_comp = AS.T_an(x_comp, t[j], k/rhoc, 100)
    plt.plot(x, T, linewidth=2, label="Analytical solution " + str(t[j]) + " s")
    # plt.plot(x_comp, T_comp, linewidth=2, label="Analytical solution " + str(t[j]) + " s", marker="*")

    # for i in range(len(alpha)): # Uncomment this to include FE solution and comment line below
    for i in range(1, len(alpha)):  # Uncomment this to exclude FE solution and comment line above
        sim = mesh.Mesh1D(n_el=n_el,
                      x_start=0,
                      x_end=0.02,
                      d_init=d_initial,
                      rhoc=rhoc,
                      k=k,
                      S=0,
                      T_L=None,
                      T_R=0,
                      q_L=0,
                      q_R=None,
                      alpha=alpha[i],
                      CFL=CFL,
                      t_final=t[j])
        sim.solve(3)
        error = np.array([T_comp - sim.d.flatten()]).T
        print(np.sum(np.abs(error)))
        err[j, i] = np.sqrt(np.matmul(error.T, np.matmul(sim.M/sim.rhoc, error)))[0, 0]
        plt.plot(sim.x_nodes, sim.d, label=names[i] + " " + str(t[j]) + " s", linestyle=style[i], marker=markers[i], markerfacecolor="None", linewidth=2)
    print("_______________________")
print(err)

# np.savetxt("./document/tables/part21.csv", err, delimiter=' & ', fmt='%4.4g', newline=' \\\\\n')
# np.savetxt("./document/tables/part22.csv", err, delimiter=' & ', fmt='%4.4g', newline=' \\\\\n')


plt.xlim(0, 0.02)
plt.grid()
plt.legend()
plt.xlabel("x (m)")
plt.ylabel("T ($^o$C)")
plt.title("Temperature distributions generated using a time-step size of " + str(CFL) +"$\Delta t_{crit}$")
# plt.title("Temperature distributions generated using a time-step size of $\Delta t_{crit}$")
plt.figure(1).set_figheight(figsize*9)
plt.figure(1).set_figwidth(figsize*14)
plt.figure(1).tight_layout()
# plt.savefig("./document/figures/part2e.pdf")
# plt.savefig("./document/figures/part2noe.pdf")
# plt.savefig("./document/figures/part21.pdf")
plt.show()