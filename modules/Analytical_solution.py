# Script name: Analytical_solution.py
# Written by: Benjamin Alheit
# Description: Contains a function that produces the analytical solution for the temperature distribution through a thin
# plate
import numpy as np


def T_an(x, t, alpha, N):
    """
    See description above
    :param x: A numpy array of x positions
    :param t: The time at which the temperature distribution will be calculated
    :param alpha: The value of k/ (rho * c)
    :param N: The number to which the summation must run
    :return: A numpy array of temperature values at the positions of x
    """
    points = np.alen(x)     # The number of points that the temperature must be caluclated at
    L = x[-1] - x[0]        # The thickness of the plate
    T = np.zeros(points)    # Initialising an array to store the temperature values

    # Applying the summation
    for n in range(1, N+1):
        lambda_n = np.pi *(2*n-1) / (2 * L)
        T += 200*(4/np.pi) * (-1)**(n+1) / (2*n - 1) * np.e ** (-alpha * lambda_n ** 2 * t) * np.cos(lambda_n * x)
    return T
