import numpy as np


def quad_selector(n_gp):
    """
    Selects the necessary parameters for a desired Gauss quadrature
    :param n_gp: The desired number of Gauss points
    :return: The number of gauss points to be used (it will be changed if an invalid number of gauss points was
    chosen), the xi positions of the gauss points, the weights of each gauss point
    """
    # A bunch of if-else statements to determine the values required for the inputted number of gauss points
    if n_gp == 1:
        xi = np.array([0])
        weights = np.array([2])
    elif n_gp == 2:
        xi = np.array([-1 / (3 ** 0.5), 1 / (3 ** 0.5)])
        weights = np.array([1, 1])
    elif n_gp == 3:
        xi = np.array([-(3 / 5) ** 0.5, 0, (3 / 5) ** 0.5])
        weights = np.array([5 / 9, 8 / 9, 5 / 9])
    elif n_gp == 5:
        xi = np.array([-(1 / 3) * (5 - 2 * ((10 / 17) ** 0.5)) ** 0.5,
                       (1 / 3) * (5 - 2 * ((10 / 17) ** 0.5)) ** 0.5,
                       -(1 / 3) * (5 + 2 * ((10 / 17) ** 0.5)) ** 0.5,
                       (1 / 3) * (5 + 2 * ((10 / 17) ** 0.5)) ** 0.5,
                       0])

        weights = np.array([(322 + 13 * (70 ** 0.5)) / 900,
                            (322 + 13 * (70 ** 0.5)) / 900,
                            (322 - 13 * (70 ** 0.5)) / 900,
                            (322 - 13 * (70 ** 0.5)) / 900,
                            128 / 225])
    else:
        # If the inputted number of gauss points is not valid then a 5th order gauss quad is used for accuracy
        print("Invalid input for linear quadrature. Gauss points set to 5 for accuracy.")
        n_gp = 5
        xi = np.array([-(1 / 3) * (5 - 2 * ((10 / 17) ** 0.5)) ** 0.5,
                       (1 / 3) * (5 - 2 * ((10 / 17) ** 0.5)) ** 0.5,
                       -(1 / 3) * (5 + 2 * ((10 / 17) ** 0.5)) ** 0.5,
                       (1 / 3) * (5 + 2 * ((10 / 17) ** 0.5)) ** 0.5,
                       0])

        weights = np.array([(322 + 13 * (70 ** 0.5)) / 900,
                            (322 + 13 * (70 ** 0.5)) / 900,
                            (322 - 13 * (70 ** 0.5)) / 900,
                            (322 - 13 * (70 ** 0.5)) / 900,
                            128 / 225])
    return n_gp, xi, weights

class Linear:
    # A linear element object
    def __init__(self, x_start, x_end, node_nums, d_innit=None):
        """
        Constructor for a linear 1D element
        :param x_start: The position of the strat of the element
        :param x_end: The position of the end of the element
        :param node_nums: The global node number of the element
        :param d_innit: The initial conditions for the element
        """
        # Assigning parameters to be stored in the object
        self.x_start = x_start
        self.x_end = x_end
        self.l_e = x_end - x_start
        self.node_nums = node_nums
        self.n_nodes = 2

        # Initialising element level matricies and vectors
        self.K = np.zeros([2, 2])
        self.M = np.zeros([2, 2])
        if d_innit is None:
            self.d = np.zeros([2, 1])
        else:
            self.d = d_innit

        # self.d_dot = np.zeros([2, 1])
        self.F = np.zeros([2, 1])

    def J(self):
        """
        Calculates the jacobian
        :return: The value of the jacobian
        """
        return (self.x_end - self.x_start) / 2

    def xi_to_x(self, xi):
        """
        Maps xi to x
        :param xi: The value of xi
        :return: The value of x
        """
        return (self.x_end + self.x_start)/2 + xi * (self.x_end - self.x_start) / 2

    def gauss_quadrature(self, n_gp, integrand):
        """
        Uses gauss quadrature to integrate over the element
        :param n_gp: The number of gauss points
        :param integrand: The function to be integrated
        :return: The evaluation of the integral
        """
        n_gp, xi, weights = quad_selector(n_gp)
        integrated = 0
        for i in range(n_gp):
            integrated += self.J() * weights[i] * integrand(xi[i])
        return integrated

    def N(self, xi):
        """
        The shape functions of the element
        :param xi: The value of xi
        :return: A numpy array of the value shape functions at position xi
        """
        return np.array([[1-xi, xi+1]]) / 2

    def B(self, xi):
        """
        The derivative shape functions of the element
        :param xi: The value of xi
        :return: A numpy array of the value derivative shape functions at position xi
        """
        return np.array([[- 1, 1]]) / self.l_e
