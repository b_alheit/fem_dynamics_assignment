import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.linalg
from modules import Elements1D

class Mesh1D:
    def __init__(self, n_el, x_start=0, x_end=0.02, d_init=None, rhoc=10e6, k=10, S=lambda x: 0, T_L = None, T_R = None, q_L=None, q_R=None, alpha=0, CFL=0.8,
                 t_final=60, lump=False):
        """
        The constructor for a 1D transient finite element temperature simulation
        :param n_el: The number of elements to be used
        :param x_start: The starting position
        :param x_end: The end position
        :param d_init: An array of initial nodal values
        :param rhoc: The value of rho * c
        :param k: The value of k
        :param S: A function giving the value of a source term
        :param T_L: The temperature of the left side of the plate
        :param T_R: The temperature of the right side of the face
        :param q_L: The heat flux of the left side of the plate
        :param q_R: The heat flux of the right side of the plate
        :param alpha: The value of alpha for the alpha-scheme
        :param CFL: A scaling factor of the critical time-step to get the time-step that will be used
        :param t_final: The final time to solve to
        :param lump: A boolean deciding whether or not to use mass-lumping
        """

        # Assigning some values to be stored in the object
        self.x_start = x_start
        self.x_end = x_end
        self.n_el = n_el
        self.n_nodes = n_el + 1
        self.rhoc = rhoc
        self.k = k
        self.S = S
        self.T_L = T_L
        self.T_R = T_R
        self.q_L = q_L
        self.q_R = q_R
        self.alpha = alpha
        self.CFL = CFL
        self.dt = None
        self.t_final = t_final
        self.t = 0
        self.lambda_max = None
        self.lump = lump

        # Initialising global matricies and vectors
        self.K = np.zeros([self.n_nodes, self.n_nodes])
        self.M = np.zeros([self.n_nodes, self.n_nodes])
        self.F = np.zeros([self.n_nodes, 1])
        self.d_dot = np.zeros([self.n_nodes, 1])
        if d_init is None:
            self.d = np.zeros([self.n_nodes, 1])
        else:
            self.d = d_init
        self.matk = np.zeros([self.n_nodes, self.n_nodes])  # Initialising matrix to store (M + alpha * Delta t* K)
        self.matkinv = np.zeros([self.n_nodes, self.n_nodes])
        self.matktk = np.zeros([self.n_nodes, self.n_nodes])    # Initialising matrix to store (M - (1-alpha) * Delta t* K)
        self.F_drive = np.zeros([self.n_nodes, 1])

        self.x_nodes = np.linspace(x_start, x_end, self.n_nodes)
        if q_L is not None:
            self.F[0, 0] = q_L
        if q_R is not None:
            self.F[-1, 0] = q_R

        # Creating an array of linear 1D elements
        self.elements = np.empty(self.n_el, dtype=Elements1D.Linear)
        for i in range(self.n_el):
            self.elements[i] = Elements1D.Linear(self.x_nodes[i], self.x_nodes[i+1], np.array([i, i+1]), self.d[i:i+1])

    def int_M_el(self, n_gp):
        """
        Calculates all of the element level mass matricies
        :param n_gp: Number of gauss points to be used
        """
        for i in range(self.n_el):
            e = self.elements[i]
            integrand = lambda xi: self.rhoc * np.matmul(e.N(xi).T, e.N(xi))
            e.M = e.gauss_quadrature(n_gp, integrand)

    def int_K_el(self, n_gp):
        """
        Calculating the element level stiffness matricies
        :param n_gp: Number of gauss points to use
        """
        for i in range(self.n_el):
            e = self.elements[i]
            integrand = lambda xi: self.k * np.matmul(e.B(xi).T, e.B(xi))
            e.K = e.gauss_quadrature(n_gp, integrand)

    def assemble(self):
        """
        Assembles the global mass and stiffness matricies from the element matricies
        """
        self.K *= 0
        self.M *= 0
        for i in range(self.n_el):
            e = self.elements[i]
            for j in range(e.n_nodes):
                self.K[e.node_nums[j], e.node_nums] += e.K[j, :]
                self.M[e.node_nums[j], e.node_nums] += e.M[j, :]

    def make_alpha_step_matricies(self):
        """
        Precalculates matricies used for alpha scheme
        """
        self.matk = self.M + self.alpha * self.dt * self.K
        self.matkinv = np.linalg.inv(self.matk)
        self.matktk = self.matk - self.dt * self.K
        self.F_drive = self.F * self.dt

    def g(self, d=None):
        """
        This method is not required for the assignment. It was just used for debugging.
        :param d: An array of temperature values
        :return: The value of d_dot
        """
        return np.linalg.solve(self.M, self.F - np.matmul(self.K, d))

    def applyBCs(self):
        """
        Applies temperature boundary conditions
        """
        if self.T_L is not None:
            self.d[0, 0] = self.T_L

        if self.T_R is not None:
            self.d[-1, 0] = self.T_R

    def calc_dt(self):
        """
        Calculates the value of the time-step size
        """
        self.lambda_max = np.max(scipy.linalg.eigvals(self.K, self.M)).real
        # print("sys lambda max: ", self.lambda_max)
        # print("el lambda max: ", np.max(scipy.linalg.eigvals(self.elements[0].K, self.elements[0].M)).real)
        if self.alpha < 0.5:
            self.dt = self.CFL * 2 / ((1-2*self.alpha) * self.lambda_max)
        else:
            # If 0.5 \leq \alpha the Foward Euler version of the equation is used (i.e. as if alpha=0)
            self.dt = self.CFL * 2 / self.lambda_max
        # print("dt = ", self.dt)

    def alpha_step(self):
        """
        Applies a time-step using the alpha-scheme
        """
        # In the case of the assignment F_drive is a zero vector
        RHS = self.F_drive + np.matmul(self.matktk, self.d)
        self.d = np.matmul(self.matkinv, RHS)

    def special_mass_lump(self):
        """
        Lumps the mass matrix with special mass lumping
        :return:
        """
        m = np.sum(self.M)
        s = np.trace(self.M)
        a = m/s
        for i in range(self.n_nodes):
            new_val = a * self.M[i, i]
            self.M[i, :] *= 0
            self.M[i, i] = new_val

    def solve(self, n_gp):
        """
        Solves the problem to the time t_final
        :param n_gp: The number of gauss points to use for integration
        """
        self.int_K_el(n_gp)  # Integrate element stiffness matricies
        self.int_M_el(n_gp)  # Integrate element mass matricies
        self.assemble()  # Assemble global matricies
        if self.lump:   # Applies mass lumping if the lump boolean is set to True
            # print("tot before: ", np.sum(self.M))
            self.special_mass_lump()
            # print("tot after: ", np.sum(self.M))
        self.calc_dt()  # Calculates the time-step to be used
        self.make_alpha_step_matricies()    # Constructs alpha-scheme matricies
        count = 0   # Variable to count the number of iterations
        while self.t < self.t_final:
            self.applyBCs()  # Applying boundary conditions
            self.alpha_step()   # Applying an alpha step
            count += 1
            self.t += self.dt
        self.applyBCs()

    def plot(self):
        plt.plot(self.x_nodes, self.d, marker="o", markerfacecolor="None")
        plt.grid()
        plt.show()

